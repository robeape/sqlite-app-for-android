package com.example.assingment2lite;

import android.provider.BaseColumns;

public class StudentMarksContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private StudentMarksContract() {}

    // Inner class that defines the table contents
    public static class StudentMarks implements BaseColumns {
        public static final String TABLE_NAME = "studentMarks";
        public static final String COLUMN_FIRST_NAME = "firstName";
        public static final String COLUMN_LAST_NAME = "lastName";
        public static final String COLUMN_MARK = "mark";
    }
}
