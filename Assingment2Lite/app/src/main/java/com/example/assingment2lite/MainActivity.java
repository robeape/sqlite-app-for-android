package com.example.assingment2lite;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //instances of buttons and edit texts
    Button btnAdd, btnView, btnDel, btnUpdate;
    EditText edtFName, edtLName, edtMark, edtId;
    Integer mark, id;
    Long newRowId;

    DatabaseHandler dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //linking the instances with the controls in the layout
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnView = (Button) findViewById(R.id.btnView);
        btnDel = (Button) findViewById(R.id.btnDelete);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        edtFName = (EditText) findViewById(R.id.edtFirstName);
        edtLName = (EditText) findViewById(R.id.edtLastName);
        edtMark = (EditText) findViewById(R.id.edtMarks);
        edtId = (EditText) findViewById(R.id.edtID);
        id = 0;
        mark = 0;
        dbHelper = new DatabaseHandler(getApplicationContext());

        //focusing in the first field of the screen
        edtFName.requestFocus();

        //adding records through add button
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checking if the fields are filled
                CheckFields();

                //inserting into table
                Long studentId = InsertStudentMark();

                //changing long to integer the last row/id inserted
                Integer intStdId = studentId != null ? studentId.intValue() : null;

                Toast.makeText(getApplicationContext(), "The data was successfully added",
                        Toast.LENGTH_LONG).show();

            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checking if there is an Id in the input
                CheckId();

                try {
                    id = Integer.parseInt(edtId.getText().toString());
                } catch (Exception e) {
                    StatusMessage("Error", "Only numbers are accepted as ID.");
                    return;
                }

                //checking if it is a valid id (if there is a student in the table with that id)
                Cursor stdMark = GetStudentMark(id);

                //if the cursor is 0, there is no student
                if (stdMark.getCount() == 0) {
                    StatusMessage("Error", "No Data");
                    return;
                }

                while(stdMark.moveToNext()) {
                    //if the field is empty, use the data from the table
                    String firstName = edtFName.getText().toString().equals("") ?
                            stdMark.getString(stdMark.getColumnIndexOrThrow(StudentMarksContract.
                                    StudentMarks.COLUMN_FIRST_NAME)) :
                            edtFName.getText().toString();

                    String lastName = edtLName.getText().toString().equals("") ?
                            stdMark.getString(stdMark.getColumnIndexOrThrow(StudentMarksContract.
                                    StudentMarks.COLUMN_LAST_NAME)) :
                            edtLName.getText().toString();

                    String mark = edtMark.getText().toString().equals("") ?
                            stdMark.getString(stdMark.getColumnIndexOrThrow(StudentMarksContract.
                                    StudentMarks.COLUMN_MARK)) :
                            edtMark.getText().toString();

                    UpdateStudentMark(id, firstName, lastName, mark);
                }

                stdMark.close();

            }
        });

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checking if there is an Id in the input
                CheckId();

                try {
                    id = Integer.parseInt(edtId.getText().toString());
                } catch (Exception e) {
                    StatusMessage("Error", "Only numbers are accepted as ID.");
                    return;
                }

                //checking if it is a valid id (if there is a student in the table)
                Cursor stdMark = GetStudentMark(id);

                //if the cursor is 0, there is no student
                if (stdMark.getCount() == 0) {
                    StatusMessage("Error", "No Data");
                    return;
                }

                DeleteStudent();
            }
        });

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if the user did not give a specific id, show all the data in the db by setting the id=0
                if(edtId.getText().length() == 0)
                    id = 0;
                else {
                    try {
                        id = Integer.parseInt(edtId.getText().toString());
                    } catch (Exception e) {
                        StatusMessage("Error", "Only numbers are accepted as ID.");
                        return;
                    }
                }

                //showing the data
                ShowData(id);
            }
        });
    }
        //function to check if the fields are filled correct
        private void CheckFields () {
            String message = "Please fill the fields:";
            boolean error = false;

            if (edtFName.length() < 2) {
                message += " First Name";
                error = true;
            }
            if (edtLName.length() < 2) {
                message += " Last Name";
                error = true;
            }
            if (edtMark.getText().equals("")) {
                message += " Marks";
                error = true;
            }

            try {
                mark = Integer.parseInt(edtMark.getText().toString());
            } catch (Exception e) {
                message += " Marks only accept numbers";
                error = true;
            }

            if (error)
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }

        //checking if there is an ID in the input
        private void CheckId () {
            String message = "Notice: ";
            boolean error = false;

            if (edtId.getText().equals("")) {
                message += " Fill the ID";
                error = true;
            }

            if (error)
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }

        //function to update information of a specific student by using ID
        private void UpdateStudentMark ( int id, String firstName, String lastName, String mark){
            // Gets the data repository in write mode
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(StudentMarksContract.StudentMarks.COLUMN_FIRST_NAME, firstName);
            values.put(StudentMarksContract.StudentMarks.COLUMN_LAST_NAME, lastName);
            values.put(StudentMarksContract.StudentMarks.COLUMN_MARK, mark);

            // Which row to update, based on the title
            String selection = StudentMarksContract.StudentMarks._ID + " LIKE ?";
            String[] selectionArgs = { String.valueOf(id) };

            int count = db.update(
                    StudentMarksContract.StudentMarks.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs);

            //if count > 0 so the record has been updated
            if(count>0)
                Toast.makeText(getApplicationContext(), "Record has been successfully updated.",
                        Toast.LENGTH_LONG).show();
            else
                Toast.makeText(getApplicationContext(), "It was not possible to update the record. Try again later.",
                        Toast.LENGTH_LONG).show();
        }

        //function to insert a new student mark
        private Long InsertStudentMark () {

            // Gets the data repository in write mode
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(StudentMarksContract.StudentMarks.COLUMN_FIRST_NAME, edtFName.getText().toString());
            values.put(StudentMarksContract.StudentMarks.COLUMN_LAST_NAME, edtLName.getText().toString());
            values.put(StudentMarksContract.StudentMarks.COLUMN_MARK, mark);

            // Insert the new row, returning the primary key value of the new row
            newRowId = db.insert(StudentMarksContract.StudentMarks.TABLE_NAME, null, values);

            if (newRowId == -1)
                StatusMessage("Error", "Something went wrong. Please, try again later.");

            //returning the row id created to be able to show the information that was inserted
            return newRowId;
        }

        //function to get information from student(s)
        private Cursor GetStudentMark (int id){
            //database will only be used for read
            SQLiteDatabase db = dbHelper.getReadableDatabase();

            // Define a projection that specifies which columns from the database
            // you will actually use after this query.
            String[] projection = {
                    BaseColumns._ID,
                    StudentMarksContract.StudentMarks.COLUMN_FIRST_NAME,
                    StudentMarksContract.StudentMarks.COLUMN_LAST_NAME,
                    StudentMarksContract.StudentMarks.COLUMN_MARK
            };

            // Sorting the results by id
            String sortOrder =
                    StudentMarksContract.StudentMarks._ID;

            //if the id=0, show all the data in the db
            if (id==0)
                return db.query(
                        StudentMarksContract.StudentMarks.TABLE_NAME,   // The table to query
                        projection,             // The array of columns to return (pass null to get all)
                        null,              // The columns for the WHERE clause
                        null,          // The values for the WHERE clause
                        null,                   // don't group the rows
                        null,                   // don't filter by row groups
                        sortOrder               // The sort order
                );
            else
            {
                // Filter results WHERE "id" = received id
                String selection = StudentMarksContract.StudentMarks._ID + " = ?";
                String[] selectionArgs = {String.valueOf(id)};

                return db.query(
                        StudentMarksContract.StudentMarks.TABLE_NAME,   // The table to query
                        projection,             // The array of columns to return (pass null to get all)
                        selection,              // The columns for the WHERE clause
                        selectionArgs,          // The values for the WHERE clause
                        null,                   // don't group the rows
                        null,                   // don't filter by row groups
                        sortOrder               // The sort order
                );
            }
        }

        //function to build the object to be shown
        private void ShowData (Integer id){
            //getting the data and returning
            Cursor stdMark = GetStudentMark(id);

            //if there is no data returned
            if (stdMark.getCount() == 0) {
                StatusMessage("Error", "No data found!");
                return;
            }

            //getting the data returned, put in a buffer to show the alert message
            StringBuffer bfr = new StringBuffer();
            while(stdMark.moveToNext()){
                //stdMark.moveToFirst();
                bfr.append("Student Data\n");
                bfr.append("Id: " + stdMark.getString(0) + "\n");
                bfr.append("First Name: " + stdMark.getString(1) + "\n");
                bfr.append("Last Name: " + stdMark.getString(2) + "\n");
                bfr.append("Mark: " + stdMark.getString(3) + "\n\n");
            }
            StatusMessage("Data", bfr.toString());
            stdMark.close();
        }

        //function to show messages using alert dialog and it also builds the alert dialog
        private void StatusMessage (String title, String resultContent){
            //showing messages as alert dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //is not possible to cancel the massage
            builder.setCancelable(true);
            builder.setTitle(title);
            builder.setMessage(resultContent);
            builder.show();
        }

        //function used to delete a specific student using ID
        private void DeleteStudent (){
            // Gets the data repository in write mode
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            // Define 'where' part of query.
            String selection = StudentMarksContract.StudentMarks._ID + " LIKE ?";
            // Specify arguments in placeholder order.
            String[] selectionArgs = { edtId.getText().toString() };
            // Issue SQL statement.
            int deletedRows = db.delete(StudentMarksContract.StudentMarks.TABLE_NAME, selection, selectionArgs);

            //confirming that the data was deleted successfully
            String message = deletedRows + " row deleted from database. \nID = " + edtId.getText().toString();
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

        }

        @Override
        protected void onDestroy() {
            //once the app is closed, the connection with database is closed
            dbHelper.close();
            super.onDestroy();
        }
}
